# clojure [![Build Status](https://travis-ci.org/alexflav23/clojure.svg)](https://travis-ci.org/alexflav23/clojure) [![Coverage Status](https://coveralls.io/repos/alexflav23/clojure/badge.svg?branch=master)](https://coveralls.io/r/alexflav23/clojure?branch=master)

Set of Euler problems solved in Clojure for the purpose of learning the language.
