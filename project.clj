(defproject learning "0.0.1"
  :description "An introduction to clojure programming"
  :dependencies [
                  [org.clojure/test.check "0.7.0"]
                  [org.clojure/test.generative "0.5.2"]
                  [prismatic/schema "0.4.0"]
                  ]

  :profiles {:dev {
                    :dependencies [[midje "1.6.3"]]
                    :plugins [ [lein-cloverage "1.0.2"]
                              [jonase/eastwood "0.2.1"]
                              [lein-midje "3.1.3"]
                              ]}}
  :main learning.runner)

