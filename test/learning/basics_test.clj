(ns learning.basics-test
  (:require
      [midje.sweet :refer :all]
      [learning.basics :as basics]
      [clojure.test :refer :all]
      [clojure.test.check :as tc]
      [clojure.test.check.generators :as gen]
      [clojure.test.check.properties :as prop]
      [clojure.test.check.rose-tree :as rose]
      ))


(deftest simple-summation
  (is (= 15 (basics/sum 1 2 3 4 5)))
  )
;;=> #'testing/simple-sumation

(deftest addition
  (is (= 4 (+ 2 2)))
  (is (= 7 (+ 3 4))))
;;=> #'testing/addition

(deftest subtraction
  (is (= 1 (- 4 3)))
  (is (= 3 (- 7 4))))
;;=> #'testing/subtraction

;;=> Testing testing


;;233168
(deftest factbased
  (fact "should compute the sum of the first numbers from 0 to 1000"

    (+ 2 2) => 4

    )
  )