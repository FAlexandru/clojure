(ns learning.4clojure-test
  (:require
    [midje.sweet :refer :all]
    [learning.4clojure :as exercises :refer :all]
    [clojure.test :refer :all]
    [clojure.test.check :as tc]
    [clojure.test.check.generators :as gen]
    [clojure.test.check.properties :as prop]
    [clojure.test.check.rose-tree :as rose]
    ))

(deftest factbased
  (fact "always return true when calling nothing-but-the-truth"
    exercises/truth => true
    )
  )

(deftest factbased
  (fact "always return true when comparing the same name value"
    exercises/simplemath => true
    )
  )

(deftest factbased
  (fact "always return true when comparing the same name value"
    exercises/stringevaluator => true
    )
  )