(ns learning.euler.problem1
  (:require [schema.core :as s
             ]))


(s/defn problem1 []
  (apply + (filter #(zero? (min (mod % 3) (mod % 5))) (range 1000)))
  )