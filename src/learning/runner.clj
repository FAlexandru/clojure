(ns learning.runner
  (:require
    [learning.basics :as basics]))


(defn -main [& args]
  (basics/hello "testing")

  ;; This will print the result of a varargs* call to the sum function.
  ;; Albeit identical to the default Clojure implementation, it's the way to pass var args.
  (println (basics/sum 1 2 3 4 5)))

