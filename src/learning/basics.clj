(ns learning.basics
  (:require [schema.core :as s
             ]))

(s/defn hello [name] :- s/Str
  (println "Hello," name))

;; This will invoke the default sum function with the same arguments passed to the sum function.
(defn sum [& args]
  (apply + args))