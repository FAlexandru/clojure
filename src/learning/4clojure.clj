(ns learning.4clojure)

(defn truth []
  (= true true)
  )

(defn simplemath []
  (= (- 10 (* 2 3)) 4)
  )

(defn stringevaluator []
  (= "HELLO WORLD" (.toUpperCase "hello world"))
  )